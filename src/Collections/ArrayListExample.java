package Collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {
    public static final int NUM_OF_ELEMENTS = 5;
    public static void main(String[] args) {
        // Sơ lược về ưu điểm của ArrayList
        // - Mảng bình thưởng sẽ có kích thước cố định không thể thay đổi
        // - ArrayList sử dụng mảng động để lưu trữ phần tử nghĩa là:
        // - Kích cỡ sẽ tăng hoặc giảm theo số lượng phần tử
        // - Có thể chứa các phần tử trùng lặp
        // - Thao tác thêm xoá chậm vì nhiều sự dịch chuyển để tối ưu bộ nhớ
        // Ta khởi tạo sức chứa cho mảng là 3
        List<Person> list1 = new ArrayList<>(NUM_OF_ELEMENTS);
        System.out.println("Capacity khởi tạo ban đầu là: "+NUM_OF_ELEMENTS);
        for(int i = 1;i<=7;i++){
            Person person = new Person(i, "Ten"+i);
            list1.add(person);
        }
        printList(list1);
    }
    public static void printList(List<Person> list){
        for(Person item :list){
            System.out.println(item);
        }
        System.out.println("Kích thước sau khi thêm phần tử quá capacity: ");
        System.out.println(list.size());
        System.out.println("Kết luận: mảng tự động tăng lên khi số phần tử tăng");
    }
}
