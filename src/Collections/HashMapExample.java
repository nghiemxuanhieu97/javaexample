package Collections;

import java.util.HashMap;

public class HashMapExample {
    // Gần giống với ArrayList nhưng HashMap lưu trữ giá trị theo cặp key/value
    // so sánh '==' chỉ áp dụng cho kiểu giá trị biến nguyên thuỷ
    // so sánh equal áp dụng cho kiểu đối tượng
    // hashcode là giá trị định danh cho một đối tượng, hai đối tượng bằng nhau sẽ có
    // hashcode bằng nhau còn hashcode bằng nhau thì chưa chắc 2 đối tượng đó bằng nhau
    public static void main(String[] args) {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("MSSV1", "Nguyễn Văn An");
        user.put("MSSV2", "Nguyễn Văn B");
        user.put("MSSV3", "Nguyễn Văn An");
        // Khi so sánh 2 đối tượng, equal() trả về true nếu 2 đối tượng có giá trị giống nhau
        // và trả về false nếu 2 đối tượng có giá trị khác nhau
        System.out.println("Ta so sánh sinh viên 1 và sinh viên 3 trùng tên");
        System.out.println(user.get("MSSV1").equals(user.get("MSSV3")));
        System.out.println("Ta so sánh sinh viên 1 và sinh viên 2 khác tên");
        System.out.println(user.get("MSSV1").equals(user.get("MSSV2")));
        System.out.println("Ta so sánh mã băm của từng sinh viên:");
        System.out.println(user.get("MSSV1").hashCode());
        System.out.println(user.get("MSSV2").hashCode());
        System.out.println(user.get("MSSV3").hashCode());
        System.out.println("Kết luận: những sinh viên có cùng tên hoặc cặp key value có cùng value thì hashcode() của chúng bằng nhau");

    }

}
