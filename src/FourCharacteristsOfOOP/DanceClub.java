package FourCharacteristsOfOOP;

public interface DanceClub {
    String sing();
    String dance();
    String rule();
}
