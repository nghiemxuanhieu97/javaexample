package FourCharacteristsOfOOP;

public class EmployeeOffice extends Person{
    //Subclass Employee can use or not use method work() and play()
    boolean Fat;
    protected EmployeeOffice(String name, double height, double weight, boolean fat) {
        super(name,height,weight);
        Fat = fat;
    }

    protected boolean isFat() {
        return Fat;
    }

    @Override
    protected String eat() {
        return "ăn nhiều";
    }

    @Override
    protected String sleep() {
        return "ngủ nhiều";
    }


    protected String description() {
            return "Nhân viên văn phòng: "+getName()+"\n"
                    +"Cao: "+ getHeight() + " cm\n"
                    +"Nặng: "+ getWeight()+ " kg\n"
                    +"Vì: "+ eat()+", "+sleep()+"\n"
                    +"Nên: "+(isFat()?"mập":"gầy");


    }




}
