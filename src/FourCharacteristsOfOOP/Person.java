package FourCharacteristsOfOOP;

public abstract class Person {
    // Basic properties of a person
    private String name;
    private double weight;
    private double height;

    protected String getName() {
        return name;
    }
    protected void setName(String name) {
        this.name = name;
    }
    protected double getHeight() {
        return height;
    }
    protected void setHeight(double height) {
        this.height = height;
    }
    protected double getWeight() {
        return weight;
    }
    protected void setWeight(double weight) {
        this.weight = weight;
    }

    public Person(String name, double height, double weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    //Class abstract can include abstract method and normal method
    //Subclass will override abstract method, can use are not use normal method
    protected static String work(){
        return "do something";
    }
    protected static String play(){
        return "play something";
    }
    protected abstract String eat();
    protected abstract String sleep();
    protected abstract String description();
}
