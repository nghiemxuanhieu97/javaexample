package FourCharacteristsOfOOP;

public class Main {
    public static void main(String[] args) {
        Person khang = new EmployeeOffice("Khang",160,100,true);
        EmployeeOffice hung = new EmployeeIT("Hung",150,45,false,true);
        System.out.println(khang.description());
        System.out.println();
        System.out.println(hung.description());
        //Kết luận:
        // abstract
        // - có 2 loại method là abstract method và normal method: abstract bắt buộc lớp con phải override còn normal thì không bắt
        // - 1 subclass chỉ có thể extends 1 abstrac class
        // - super(): gọi lại phương thức của lớp cha
        // - thường để chỉ sự kế thừa
        // - giả sử mỗi method là một tính năng thì normal method dùng để chỉ tính năng cung cho các subclass
        // interface
        // - mọi method của interface bắt buôc subclass implements nó phải override
        // - 1 subclass có thể implements nhiều interface
        // - thường để chỉ việc triển khai một hoạt động nào đó, kiểu plugin gắn thêm một module chức năng cho hệ thống
        // polymorshism
        // - Sự đa hình thể hiện qua demo trên dựa vào mỗi quan hệ của các class phân cấp
        // encapsulation
        // - thể hiện qua các từ khoá như public, protected, static, private...
        // Thắc mắc:
        // - tại sao khi subclass2 extends subclass1 thì nó không override lại các abstract method của supperclass ??
        // - điều này có thể gây ra việc không chuẩn hoá khi sử dụng kế thừa
        // - tại sao @Override bỏ đi thì method vẫn có thể override
    }
}



