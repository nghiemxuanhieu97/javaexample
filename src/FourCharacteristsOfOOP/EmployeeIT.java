package FourCharacteristsOfOOP;
//When EmployeeIT extends EmployeeOffice, it don't override auto eat(), sleep(), description() ??
public class EmployeeIT extends EmployeeOffice implements DanceClub {
    boolean myopic;

    public boolean isMyopic() {
        return myopic;
    }

    public void setMyopic(boolean myopic) {
        this.myopic = myopic;
    }

    public EmployeeIT(String name, double height, double weight, boolean fat, boolean myopic) {
        super(name, height, weight, fat);
        this.myopic = myopic;
    }

    @Override
    public String eat() {
        return "ăn ít";
    }

    @Override
    public String sleep() {
        return "ngủ ít";
    }

    @Override
    public String sing() {
        return "hát";
    }

    @Override
    public String dance() {
        return "nhảy";
    }

    @Override
    public String rule() {
        return "Tham gia câu lạc bộ Dance phải biết: "+dance() +" và "+ sing();
    }


    public String description() {

            return "Nhân viên IT: " + getName() + "\n"
                    + "Cao: " + getHeight() + " cm\n"
                    + "Nặng: " + getWeight() + " kg\n"
                    + "Vì: " + eat() + ", " + sleep() + "\n"
                    + "Nên: " + (isFat() ? "mập" : "gầy")+ "\n"
                    + "Thường: "+ (isMyopic()?"bị cận":"không bị cận")+ "\n"
                    + rule();



    }
}
