package Exceptions;

public class Driver {
    public static boolean wearHelmet;

    public Driver(boolean wearHelmet) {
      this.wearHelmet = wearHelmet;
    }

    public static boolean isWearHelmet() {
        return wearHelmet;
    }

}
