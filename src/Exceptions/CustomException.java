package Exceptions;

public class CustomException {
    public static void main(String[] args) throws DontWearHelmetException {
        TrafficService sv = new TrafficService();
        Driver hoang = new Driver(false);
        sv.wearHelmet(hoang.isWearHelmet());


    }
    public static class TrafficService{
        public boolean wearHelmet(boolean w) throws DontWearHelmetException {
            if(Driver.isWearHelmet() == false){
                throw new DontWearHelmetException("Don't wear Helmet");
            }
            return true;
        }
    }
}
