package Exceptions;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class UncheckedException {
    //Một số UncheckedException:
    //- NullPointerException
    //- ArrayIndexOutOfBoundsException
    //- ArithmeticException
    //- NumberFormatException

    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 0;
        try{int result = num1/num2;
            System.out.println(result);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Đây là ví dụ uncheckedException");
        };

    }
}
