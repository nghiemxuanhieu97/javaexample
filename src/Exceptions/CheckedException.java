package Exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedException {
    public static void main(String[] args) {
        try{
            FileReader file = new FileReader("a.txt");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
            System.out.println("Đây là ví dụ cho checked exception. Hệ thống không tìm thấy file theo đường dẫn");
        }
    }
}
