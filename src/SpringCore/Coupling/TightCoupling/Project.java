package SpringCore.Coupling.TightCoupling;

public class Project {
    public static void main(String[] args) {
        String sql = "SELECT * FROM TABLE";
        Connection cnn = new Connection();
        cnn.connectHandle(sql);
        //Ở đây giả sử ta thay đổi qua JDBC thì sẽ thay đổi class Connection

    }



}
