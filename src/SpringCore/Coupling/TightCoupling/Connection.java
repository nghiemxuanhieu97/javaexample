package SpringCore.Coupling.TightCoupling;

public class Connection {
    ODBC odbc = new ODBC();
    public Connection(){

    }
    public void connectHandle(String sql){
        odbc.connect(sql);
    }
}
