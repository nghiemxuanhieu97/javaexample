package SpringCore.Coupling.LooseCoupling;
public class JDBC implements DataBase{
    @Override
    public void connect(String sql) {
        //Add logic here
        System.out.println("Connected sucessfully with JDBC.\n" +
                "Your sql is: "+sql);
    }
}
