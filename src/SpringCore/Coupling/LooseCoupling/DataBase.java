package SpringCore.Coupling.LooseCoupling;

public interface DataBase {
    public void connect(String sql);
}
