package SpringCore.Coupling.LooseCoupling;


public class Project {
    public static void main(String[] args) {
        String sql = "Select * FROM TABLE";
//        Nếu ta muốn thay đổi qua ODBC ta chỉ cần khởi tạo
//        chứ không phải sửa các thứ trong class Connection
//        DataBase odbc = new ODBC();
//        Connection conn = new Connection(odbc);
        DataBase jdbc = new JDBC();
        Connection conn = new Connection(jdbc);
        conn.connectHandle(sql);

    }
}
