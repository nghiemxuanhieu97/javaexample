package SpringCore.Coupling.LooseCoupling;

public class ODBC implements DataBase{
    @Override
    public void connect(String sql) {
        //add logic here
        System.out.println("Connected sucessfully with ODBC.\nYour sql is: "+sql);
    }
}
