package SpringCore.Coupling.LooseCoupling;

public class Connection {
    private DataBase db;
    public Connection(DataBase db){
        this.db = db;
    }
    public void connectHandle(String sql){
        db.connect(sql);
    }

}
