package String;

public class StringExample {


    public static void main(String[] args) {
        //Ta có thể khởi tạo biến mang kiểu dữ liệu là String bằng 2 cách, nhưng thường dùng cách một.
        //Vì chuỗi String là bất biến nên ta không thể thay đổi giá trị khi đã gán cố định cho một biến
        String chuoi1 = "String là một đối tượng";
        String chuoi2 = new String(" trong Java nên có thể khởi tạo được!");
        String chuoi3 = "String là một đối tượng";
        String chuoi4 = new String("String là một đối tượng");
        //Đối tượng String cung cấp các StringAPI qua các phương thức như concat() dùng để nối chuỗi
        String result = chuoi1.concat(chuoi2);
        System.out.println(result);
        //Khi so sánh chuỗi về mặt nội dung ta dùng phương thức equal()
        System.out.println("Đây là kết quả khi so sánh nội dung chuỗi 1 và chuỗi 3 bằng method equal()");
        System.out.println(chuoi1.equals(chuoi3));
        //Khi so sánh giá trị ô nhớ ta dùng toán tử "=="
        System.out.println("Đây là kết quả khi so sánh chuỗi 1 và chuỗi 3 bằng toán tử '=='");
        System.out.println(chuoi1==chuoi3);
        System.out.println("Đây là kết quả khi so sánh chuỗi 1 và chuỗi 4 bằng toán tử '=='");
        System.out.println(chuoi1==chuoi4);
        System.out.println("Có thể thấy rằng vì chuỗi 4 được khởi tạo một ô nhớ mới nên khi so sánh sẽ ra false");
        StringBuilder chuoi5 = new StringBuilder("Đây là một chuỗi có thể thay đổi do sử dụng đối tượng StringBuilder để khai báo");
        chuoi5 = chuoi5.append(". Đối tượng StringBulder cho phép ta thay đổi nội dung biến");
        System.out.println("Đây là chuỗi 5 sau khi dùng phương thức append():\n "+ chuoi5);

    }
}
